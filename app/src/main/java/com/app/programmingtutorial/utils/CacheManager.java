package com.app.programmingtutorial.utils;

import android.app.Application;
import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.IOException;

public class CacheManager extends Application {

	static String PACKAGE_NAME;
	static String CACHE_DIRECTORY;

	@Override
	protected void attachBaseContext(Context context) {
		super.attachBaseContext(context);
	}

	@Override
	public void onCreate() {

		PACKAGE_NAME = getApplicationContext().getPackageName();

		if (Environment.MEDIA_MOUNTED.equals(Environment
				.getExternalStorageState()))
			CACHE_DIRECTORY = Environment.getExternalStorageDirectory()
					.getAbsolutePath()
					+ File.separator
					+ "Android"
					+ File.separator
					+ "data"
					+ File.separator
					+ PACKAGE_NAME
					+ File.separator
					+ "cache"
					+ File.separator
					+ AppUtils.getVersionCode(getApplicationContext())
					+ File.separator;
		else
			CACHE_DIRECTORY = this.getCacheDir().getPath() + File.separator
					+ AppUtils.getVersionCode(getApplicationContext())
					+ File.separator;

		getDataCacheDir();

		super.onCreate();
	}

	public static String getDataCacheDir() {

		String dir = CACHE_DIRECTORY + "data" + File.separator;
		new File(dir).mkdirs();
		try {
			new File(dir, ".nomedia").createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block

		}
		return dir;
	}

}
