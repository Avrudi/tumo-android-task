package com.app.programmingtutorial.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.SystemClock;
import android.util.Log;
import android.view.InputDevice;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.app.programmingtutorial.R;
import com.google.android.material.snackbar.Snackbar;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class Utility {

	public static boolean isNetworkConnectionAvailableWithoutText(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService( Context.CONNECTIVITY_SERVICE );
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		return netInfo != null && netInfo.isConnected();
	}

	public static void showSnackBar(String text, View mViewPager, Context context) {
		try {
			Snackbar mSnackBar = Snackbar.make( mViewPager, text, Snackbar.LENGTH_LONG );

			View snackBarView = mSnackBar.getView();
			snackBarView.setBackgroundColor( context.getResources().getColor( R.color.colorText ) );

			TextView snackText = (TextView) (mSnackBar.getView()).findViewById( com.google.android.material.R.id.snackbar_text );
			snackText.setTextColor( context.getResources().getColor( R.color.white ) );

			mSnackBar.show();
		} catch (Error ignore){}
	}

	public static class CustomWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			try {
				view.loadUrl( url );
				return true;
			} catch (Exception e) {
				Log.e("hi", "" + e);
			}
			return false;
		}
	}





}
