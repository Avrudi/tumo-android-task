package com.app.programmingtutorial.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

public class AppUtils {

	public static int getVersionCode(Context context) {
		PackageInfo pinfo;
		int versionCode = 0;
		try {
			pinfo = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0);
			versionCode = pinfo.versionCode;
		} catch (NameNotFoundException e) {
			Log.e("PackageManager Error", "AppUntils: " + e);
		}
		return versionCode;
	}

}
