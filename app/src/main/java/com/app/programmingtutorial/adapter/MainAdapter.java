package com.app.programmingtutorial.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.programmingtutorial.R;
import com.app.programmingtutorial.activity.ExamActivity;
import com.app.programmingtutorial.activity.MainActivity;
import com.app.programmingtutorial.model.ExamObject;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.lang.reflect.Array;
import java.util.ArrayList;
import io.supercharge.shimmerlayout.ShimmerLayout;

public class MainAdapter extends ArrayAdapter<Array> {

    private final LayoutInflater inflater;
    private final Activity activity;
    private final ArrayList<ExamObject> examArray;
    private final boolean isLoading;

    public MainAdapter(Activity activity, ArrayList<ExamObject> examArray, boolean isLoading) {
        super( activity.getApplicationContext(), R.layout.row_main );

        this.examArray = examArray;
        this.activity = activity;
        this.isLoading = isLoading;

        inflater = (LayoutInflater) this.activity.getApplicationContext()
                .getSystemService( Context.LAYOUT_INFLATER_SERVICE );
    }

    @Override
    public int getCount() {
        if (isLoading)
            return 20;
        else
            return 1;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(final int position, View convertView,
                        ViewGroup parent) {

        RecordHolder holder;

        if (convertView == null) {
            holder = new RecordHolder();

            if (isLoading) {
                convertView = inflater
                        .inflate( R.layout.row_loading, parent, false );

                holder.image = convertView
                        .findViewById( R.id.imageView );


                holder.loading = convertView.findViewById( R.id.loading_view );
                holder.loading.setAnimationReversed( true );
                holder.loading.setShimmerColor( activity.getResources().getColor( R.color.colorLoading ) );
                holder.loading.setShimmerAnimationDuration( 3000 );

            } else {

                convertView = inflater
                        .inflate( R.layout.row_main, parent, false );

                holder.title = convertView.findViewById( R.id.titleTextView );
                holder.description = convertView.findViewById( R.id.descriptionTextView );
            }

            convertView.setTag( holder );

        } else {
            holder = (RecordHolder) convertView.getTag();
        }

        if (isLoading) {

            holder.loading.startShimmerAnimation();
        } else {
            ExamObject examObject = examArray.get( position );

            int examCount = position + 1;
            holder.title.setText( "Exam " + examCount );

            if (examObject.title != null)
                holder.description.setText( examObject.title );

            convertView.setOnClickListener( arg0 -> {

                if(MainActivity.testHasDone) {
                    new MaterialAlertDialogBuilder(activity)
                            .setTitle("Warning")
                            .setMessage("You have already done this quiz. Would you like to try it again?\n(just in beta version)")
                            .setPositiveButton("SURE", (dialogInterface, i) -> intentToExam() )
                            .setNegativeButton("CANCEL", (dialogInterface, i) -> {

                            } )
                            .setCancelable( false )
                            .show();
                } else {
                    intentToExam();
                }

            } );
        }

        return convertView;
    }

    private void intentToExam(){
        Bundle extra = new Bundle();
        extra.putSerializable("examArray", examArray);

        Intent i = new Intent( activity, ExamActivity.class );
        i.putExtra( "extra", extra );
        i.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
        activity.startActivity( i );
    }

    static class RecordHolder {
        TextView title;
        TextView description;
        ImageView image;
        ShimmerLayout loading;
    }

}

