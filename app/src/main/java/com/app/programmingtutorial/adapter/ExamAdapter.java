package com.app.programmingtutorial.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.app.programmingtutorial.R;
import com.app.programmingtutorial.activity.ExamActivity;
import com.app.programmingtutorial.activity.ImageDetailActivity;
import com.app.programmingtutorial.model.ExamObject;
import com.app.programmingtutorial.utils.Utility;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.lang.reflect.Array;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExamAdapter extends ArrayAdapter<Array> {

    private static final int DATA_TYPE_TEXT = 0;
    private static final int DATA_TYPE_IMAGE = 1;
    private static final int DATA_TYPE_VIDEO = 2;
    private static final int DATA_TYPE_QUIZ = 3;
    private static final int DATA_TYPE_INPUT = 4;

    private final LayoutInflater inflater;
    private final Context context;
    private final FrameLayout customViewContainer;
    private final Activity activity;

    private final DisplayImageOptions options;
    private final ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

    public ExamAdapter(Context context, FrameLayout customViewContainer, Activity activity) {
        super( context, R.layout.row_exam_text );

        this.context = context;
        this.customViewContainer = customViewContainer;
        this.activity = activity;


        inflater = (LayoutInflater) this.context
                .getSystemService( Context.LAYOUT_INFLATER_SERVICE );

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading( R.drawable.placeholder_big )
                .showImageOnFail( R.drawable.placeholder_big )
                .showImageForEmptyUri( R.drawable.placeholder_big )
                .cacheInMemory( true ).cacheOnDisk( true )
                .considerExifParams( true ).build();
    }

    @Override
    public int getViewTypeCount() {
        return 5;
    }

    @Override
    public int getItemViewType(int position) {

        if (ExamActivity.newExamObject.get( position ).type.equalsIgnoreCase( "text" )) {
            return DATA_TYPE_TEXT;
        } else if (ExamActivity.newExamObject.get( position ).type.equalsIgnoreCase( "image" )) {
            return DATA_TYPE_IMAGE;
        } else if (ExamActivity.newExamObject.get( position ).type.equalsIgnoreCase( "video" )) {
            return DATA_TYPE_VIDEO;
        } else if (ExamActivity.newExamObject.get( position ).type.equalsIgnoreCase( "input" )) {
            return DATA_TYPE_INPUT;
        } else if (ExamActivity.newExamObject.get( position ).type.equalsIgnoreCase( "quiz" )) {
            return DATA_TYPE_QUIZ;
        } else {
            return DATA_TYPE_QUIZ;
        }
    }

    @Override
    public int getCount() {
        if (ExamActivity.isVideoPlaying)
            return 0;
        else
            return ExamActivity.newExamObject.size();
    }

    @SuppressLint({"SetTextI18n", "SetJavaScriptEnabled"})
    @Override
    public View getView(final int position, View convertView,
                        ViewGroup parent) {

        RecordHolder holder;
        int listViewItemType = getItemViewType( position );

        if (convertView == null) {
            holder = new RecordHolder();

            if (listViewItemType == DATA_TYPE_TEXT) {
                convertView = inflater
                        .inflate( R.layout.row_exam_text, parent, false );

                holder.content = convertView.findViewById( R.id.contentTextView );

            } else if (listViewItemType == DATA_TYPE_IMAGE) {

                convertView = inflater
                        .inflate( R.layout.row_exam_image, parent, false );

                holder.imageView = convertView.findViewById( R.id.imageView );
                holder.imageView.getLayoutParams().height = (int) (generateMetrics( context ).widthPixels / 1.77);


            } else if (listViewItemType == DATA_TYPE_VIDEO) {
                convertView = inflater
                        .inflate( R.layout.row_exam_video, parent, false );

                holder.webView = convertView.findViewById( R.id.webView );
                holder.imageView = convertView.findViewById( R.id.imageView );
                holder.btnPlay = convertView.findViewById( R.id.btnPlay );
                holder.progressBar = convertView.findViewById( R.id.progress);

                int height = (int) (generateMetrics( context ).widthPixels / 1.6);

                holder.webView.getLayoutParams().height = height;
                holder.imageView.getLayoutParams().height = height;

                holder.webView.setWebViewClient( new Utility.CustomWebViewClient() );
                holder.webView.getSettings().setJavaScriptEnabled(true);
                holder.webView.getSettings().setMediaPlaybackRequiresUserGesture(true);
                holder.webView.setWebChromeClient( new ExamActivity.myWebChromeClient(customViewContainer, activity, this, holder.progressBar) );

            } else if (listViewItemType == DATA_TYPE_INPUT) {
                convertView = inflater
                        .inflate( R.layout.row_exam_input, parent, false );

                holder.editText = convertView.findViewById( R.id.inputEditText );
                holder.content = convertView.findViewById( R.id.contentTextView );

            } else if (listViewItemType == DATA_TYPE_QUIZ) {
                convertView = inflater
                        .inflate( R.layout.row_exam_quiz, parent, false );

                holder.content = convertView.findViewById( R.id.contentTextView );
                holder.radioGroup = convertView.findViewById( R.id.radio );

            } else {
                convertView = inflater
                        .inflate( R.layout.row_exam_text, parent, false );
            }

            holder.title = convertView
                    .findViewById( R.id.titleTextView );

            convertView.setTag( holder );

        } else {
            holder = (RecordHolder) convertView.getTag();
        }

        ExamObject examObject = ExamActivity.newExamObject.get( position );
        holder.title.setText( examObject.title );

        if (listViewItemType == DATA_TYPE_TEXT) {
            holder.content.setText( examObject.content );

        } else if (listViewItemType == DATA_TYPE_IMAGE) {

            try {
                if (examObject.content != null) {
                    ImageLoader.getInstance().displayImage(
                            examObject.content,
                            holder.imageView, options, animateFirstListener );

                    holder.imageView.setOnClickListener( v -> {
                        Intent i = new Intent(context,
                                ImageDetailActivity.class);
                        i.putExtra("imageURL", "" + examObject.content);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                    } );
                }
            } catch (Exception ignored){}


        } else if (listViewItemType == DATA_TYPE_VIDEO) {



            final Matcher matcher = Pattern.compile("v=([^\\s&#]*)", Pattern.MULTILINE).matcher(examObject.content);
            String imageUrl;
            if(matcher.find()) {

                try{
                    imageUrl = "https://img.youtube.com/vi/" + matcher.group(1) + "/hqdefault.jpg";

                    if (imageUrl != null)
                        ImageLoader.getInstance().displayImage(
                                imageUrl,
                                holder.imageView, options, animateFirstListener );
                } catch (Exception ignored){}


            }

            if (ExamActivity.status != 1 ) {
                holder.btnPlay.setOnClickListener( arg0 -> {
                    try {
                        holder.imageView.setVisibility( ViewGroup.GONE );
                        holder.btnPlay.setVisibility( ViewGroup.GONE );
                        holder.webView.setVisibility( ViewGroup.VISIBLE );
                        holder.progressBar.setVisibility( ViewGroup.VISIBLE );

                        holder.webView.loadUrl( examObject.content );
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } );
            }

        } else if (listViewItemType == DATA_TYPE_INPUT) {
            if (ExamActivity.status != 0) {
                holder.editText.setEnabled( false );
                holder.editText.clearFocus();
                holder.editText.setFocusable( false );
            }

            holder.content.setText( examObject.content );
            holder.editText.setText( ExamActivity.inputString );
            holder.editText.addTextChangedListener(new TextWatcher()
            {
                @Override
                public void afterTextChanged(Editable mEdit)
                {
                    ExamActivity.inputString = mEdit.toString();
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after){}

                public void onTextChanged(CharSequence s, int start, int before, int count){}
            });


        } else if (listViewItemType == DATA_TYPE_QUIZ){
            holder.content.setText( examObject.content );
            ExamActivity.correctAnswer = examObject.answer;

            if (holder.radioGroup.getChildCount() == examObject.choices.size()) {
                int count = holder.radioGroup.getChildCount();
                for (int i = count - 1; i >= 0; i--) {
                    View o = holder.radioGroup.getChildAt( i );
                    if (o instanceof RadioButton) {
                        holder.radioGroup.removeViewAt( i );
                    }
                }
            }

                for (int i = 0; i < examObject.choices.size(); i++) {
                    RadioButton rbn = new RadioButton( context );
                    rbn.setId( View.generateViewId() );
                    rbn.setText( examObject.choices.get( i ) );


                    rbn.setEnabled( ExamActivity.status == 0 );

                    if(ExamActivity.status == 2 ){

                        if (Integer.parseInt( ExamActivity.selectedIndex ) == i && examObject.answer != i) {
                            rbn.setTextColor( context.getResources().getColor( R.color.darkRed ) );
                        }

                        if (examObject.answer == i)
                            rbn.setTextColor( context.getResources().getColor( R.color.colorPrimaryDark ) );
                    }

                    if (!ExamActivity.selectedIndex.equalsIgnoreCase( "" )){
                        int selectedIndex =  Integer.parseInt( ExamActivity.selectedIndex );
                        rbn.setChecked( i == selectedIndex );
                    }


                    int finalI = i;
                    rbn.setOnClickListener( arg0 -> {
                        ExamActivity.selectedIndex = "" + finalI;
                        Log.e( "hi",  ExamActivity.selectedIndex);
                    } );

                    holder.radioGroup.addView( rbn );
                }


        }

        return convertView;
    }

    static class RecordHolder {
        TextView title;
        TextView content;
        ImageView imageView;
        EditText editText;
        WebView webView;
        Button btnPlay;
        ProgressBar progressBar;
        RadioGroup radioGroup;

    }

    private DisplayMetrics generateMetrics(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();

        WindowManager windowManager = (WindowManager) context
                .getSystemService( Context.WINDOW_SERVICE );
        windowManager.getDefaultDisplay().getMetrics( metrics );
        return metrics;
    }

    private static class AnimateFirstDisplayListener extends
            SimpleImageLoadingListener {

        final List<String> displayedImages = Collections
                .synchronizedList( new LinkedList<>() );

        @Override
        public void onLoadingComplete(String imageUri, View view,
                                      Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains( imageUri );
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate( imageView, 500 );
                    displayedImages.add( imageUri );
                }
            }
        }
    }

}

