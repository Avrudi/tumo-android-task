package com.app.programmingtutorial.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.app.programmingtutorial.R;
import com.app.programmingtutorial.utils.TouchImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;

public class ImageDetailActivity extends AppCompatActivity {
    DisplayImageOptions options;

    private final ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_image );

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading( R.drawable.placeholder_big )
                .showImageOnFail( R.drawable.placeholder_big )
                .showImageForEmptyUri( R.drawable.placeholder_big )
                .cacheInMemory( true ).cacheOnDisk( true )
                .considerExifParams( true ).build();

        ImageButton back = findViewById( R.id.btn_back );

        back.setOnClickListener( arg0 -> finish() );

        TouchImageView image = (TouchImageView) findViewById( R.id.img );

        ImageLoader.getInstance().displayImage(
                getIntent().getExtras().getString( "imageURL" ),
                image, options, animateFirstListener );

    }
    private static class AnimateFirstDisplayListener extends
            SimpleImageLoadingListener {

        final List<String> displayedImages = Collections
                .synchronizedList( new LinkedList<>() );

        @Override
        public void onLoadingComplete(String imageUri, View view,
                                      Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains( imageUri );
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate( imageView, 500 );
                    displayedImages.add( imageUri );
                }
            }
        }
    }

}
