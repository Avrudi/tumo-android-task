package com.app.programmingtutorial.activity;

import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.app.programmingtutorial.R;
import com.app.programmingtutorial.adapter.MainAdapter;
import com.app.programmingtutorial.core.CoreRequests;
import com.app.programmingtutorial.model.ExamObject;
import com.app.programmingtutorial.utils.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private MainAdapter adapter;
    private ListView listView;
    private Button btnTryAgain;
    private MainActivity mainActivity;

    public static boolean testHasDone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        listView = findViewById( R.id.listView );
        btnTryAgain = findViewById( R.id.btn_try_again );

        testHasDone = false;
        mainActivity = this;

        btnTryAgain.setOnClickListener( v -> fetchData() );

        fetchData();
    }

    private void fetchData(){
        if (Utility.isNetworkConnectionAvailableWithoutText( getApplicationContext() )) {
            onFetching();
            new Thread( new getExamData() ).start();
        } else {
            Utility.showSnackBar( getResources().getString( R.string.error_network ), listView, getApplicationContext());
        }
    }

    private void onFetching(){
        btnTryAgain.setVisibility( ViewGroup.GONE );
        listView.setVisibility( ViewGroup.VISIBLE );
        listView.setEnabled( false );
        adapter = new MainAdapter( mainActivity,  null , true);
        listView.setAdapter( adapter );
    }

    private void onFetchingDismiss(){
        listView.setEnabled( true );
    }

    private void onFetchingFinishedWithError(){
        btnTryAgain.setVisibility( ViewGroup.VISIBLE );
        listView.setVisibility( ViewGroup.INVISIBLE );

    }

    class getExamData implements Runnable {
        ArrayList<ExamObject> arrayList = null;

        @Override
        public void run() {

            String jsonOutput = CoreRequests.getMethod( getResources().getString( R.string.get_exam_method ),
                    getApplicationContext() );
            Gson gson = new Gson();
            Type listType = new TypeToken<List<ExamObject>>(){}.getType();

            try {
                arrayList = gson.fromJson(jsonOutput, listType);
            } catch (Exception ignored){

            }

            listView.post( () -> {
                if (arrayList != null) {
                    adapter = new MainAdapter( mainActivity,  arrayList , false);
                    listView.setAdapter( adapter );

                    onFetchingDismiss();

                } else {
                    onFetchingFinishedWithError();

                    Utility.showSnackBar( getResources().getString( R.string.error_data ), listView, getApplicationContext());
                }
            } );
        }
    }
}