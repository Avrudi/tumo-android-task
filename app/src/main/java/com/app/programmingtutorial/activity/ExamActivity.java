package com.app.programmingtutorial.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.app.programmingtutorial.R;
import com.app.programmingtutorial.adapter.ExamAdapter;
import com.app.programmingtutorial.core.CoreRequests;
import com.app.programmingtutorial.model.ExamObject;
import com.app.programmingtutorial.model.GeneralRespons;
import com.app.programmingtutorial.model.QuizObject;
import com.app.programmingtutorial.utils.Utility;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import androidx.appcompat.app.AppCompatActivity;
import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;
import br.com.simplepass.loadingbutton.customViews.OnAnimationEndListener;

@SuppressWarnings("unchecked")
public class ExamActivity extends AppCompatActivity {

    public static boolean isVideoPlaying;
    public static String selectedIndex;
    public static String inputString;
    public static int correctAnswer;
    public static int status;
    public static ArrayList<ExamObject> newExamObject;


    private CircularProgressButton btnSubmit;
    private ListView listView;
    private ExamAdapter adapter;
//    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_exam );

        manageData();
        initView();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        isVideoPlaying = false;
    }

    private void manageData(){
        isVideoPlaying = false;
        selectedIndex = "";
        inputString = "";
        status = 0;

        Bundle extra = getIntent().getBundleExtra( "extra" );
        newExamObject = (ArrayList<ExamObject>) extra.getSerializable( "examArray" );
    }

    private void initView(){
        btnSubmit = findViewById( R.id.btn_submit );
//        progressBar = findViewById( R.id.progressBar );
        listView = findViewById( R.id.listView );
        FrameLayout customViewContainer = findViewById( R.id.customViewContainer );

        adapter = new ExamAdapter( getApplicationContext(), customViewContainer, this );
        listView.setAdapter( adapter );

        btnSubmit.setOnClickListener( arg0 -> {

            if(status == 2){
                finish();

            } else {
                if (selectedIndex.equalsIgnoreCase( "" )) {
                    Utility.showSnackBar( getResources().getString( R.string.error_quiz ), listView, getApplicationContext() );
                    listView.smoothScrollToPosition( getIndexOfType( "quiz" ) );

                } else if (inputString.equalsIgnoreCase( "" )) {
                    Utility.showSnackBar( getResources().getString( R.string.error_input ), listView, getApplicationContext() );
                    listView.smoothScrollToPosition( getIndexOfType( "input" ) );
                } else {
                    btnSubmit.startAnimation();
                    status = 1;
                    adapter.notifyDataSetChanged();

                    if (Utility.isNetworkConnectionAvailableWithoutText( getApplicationContext() )) {

                        new Thread( new submitExamData() ).start();
                    } else {
                        Utility.showSnackBar( getResources().getString( R.string.error_network ), listView, getApplicationContext() );
                    }

                }
            }

        } );
    }

    private int getIndexOfType(String typeName){
        for(int i = 0; i< newExamObject.size() ; i++){
            ExamObject examObject = newExamObject.get( i );
            if (examObject.type.equalsIgnoreCase( typeName ))
                return i;
        }

        return 0;
    }

    class submitExamData implements Runnable {
        GeneralRespons generalRespons = null;

        @Override
        public void run() {

            QuizObject quizObject = new QuizObject();
            if (Integer.parseInt( selectedIndex ) == correctAnswer)
                quizObject.quiz = 1;
            else
                quizObject.quiz = 0;

            quizObject.input = inputString;

            String json = new Gson().toJson( quizObject );

            try {
                generalRespons = CoreRequests.postMethod( getResources().getString( R.string.post_exam_method ),
                        json,
                        getApplicationContext() );
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


            listView.post( () -> {
                if (generalRespons != null) {



                    btnSubmit.revertAnimation( );
                    new Timer().schedule( new TimerTask() {
                        @Override
                        public void run() {
                            btnSubmit.setText( getResources().getString( R.string.btn_continue ) );
                            btnSubmit.setBackground( getResources().getDrawable( R.drawable.rounded_button ) );
                        }
                    }, 400);

                    new MaterialAlertDialogBuilder(ExamActivity.this)
                            .setTitle("Submit")
                            .setMessage("Your answer has been submitted. Click 'dismiss' button to review your answer.")
                            .setPositiveButton("DISMISS", (dialogInterface, i) -> {

                                MainActivity.testHasDone = true;
                                status = 2;
                                adapter.notifyDataSetChanged();
                            } )
                            .setCancelable( false )
                            .show();


                } else {

                    btnSubmit.revertAnimation();
                    btnSubmit.setText( getResources().getString( R.string.btn_submit ) );

                    Utility.showSnackBar( getResources().getString( R.string.error_data ), listView, getApplicationContext());
                }
            } );
        }
    }


    public static class myWebChromeClient extends WebChromeClient {
        private View mVideoProgressView;
        private FrameLayout customViewContainer;
        private CustomViewCallback customViewCallback;
        private View mCustomView;
        private Activity activity;
        private ExamAdapter adapter;
        private ProgressBar progressBar;

        public void onProgressChanged(WebView view, int progress) {
            try {
                progressBar.animate();
                if (progress == 100) {
                    progressBar.clearAnimation();
                    progressBar.setVisibility( ViewGroup.GONE );
                }
            } catch (Exception ignored) {
            }
        }

        public myWebChromeClient(FrameLayout customViewContainer, Activity activity, ExamAdapter adapter, ProgressBar progressBar) {

            try {
                this.customViewContainer = customViewContainer;
                this.activity = activity;
                this.adapter = adapter;
                this.progressBar = progressBar;

            } catch (Exception ignored) {
            }
        }

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {

            try {
                if (mCustomView != null) {
                    callback.onCustomViewHidden();
                    return;
                }

                final Handler handler = new Handler( Looper.getMainLooper());
                handler.postDelayed( () -> {
                    activity.setRequestedOrientation( ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR );
                    mCustomView = view;
                    customViewContainer.setVisibility( View.VISIBLE );
                    customViewContainer.addView( view );
                    customViewCallback = callback;
                    isVideoPlaying = true;
                }, 10);

            } catch (Exception ignored) {
            }
        }

        @SuppressLint("InflateParams")
        @Override
        public View getVideoLoadingProgressView() {

            try {
                if (mVideoProgressView == null) {
                    LayoutInflater inflater = LayoutInflater.from( activity.getApplicationContext() );
                    mVideoProgressView = inflater.inflate( R.layout.video_progress, null );
                }
            } catch (Exception ignored) {
            }
            return mVideoProgressView;
        }

        @Override
        public void onHideCustomView() {
            super.onHideCustomView();
            try {
                if (mCustomView == null)
                    return;

                activity.setRequestedOrientation( ActivityInfo.SCREEN_ORIENTATION_PORTRAIT );
                customViewContainer.setVisibility( View.GONE );

                mCustomView.setVisibility( View.GONE );

                customViewContainer.removeView( mCustomView );
                customViewCallback.onCustomViewHidden();

                mCustomView = null;
                isVideoPlaying = false;
                adapter.notifyDataSetChanged();
            } catch (Exception ignored) {
            }
        }
    }
}