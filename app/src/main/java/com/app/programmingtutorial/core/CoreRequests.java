package com.app.programmingtutorial.core;

import android.content.Context;
import android.util.Log;

import com.app.programmingtutorial.R;
import com.app.programmingtutorial.model.GeneralRespons;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;


public class CoreRequests {

	public static String getMethod(String method, Context context) {

		String result = "";
		BufferedReader reader = null;

		try {
			URL url = new URL(context.getString(R.string.server_url) + method);

			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setConnectTimeout( 20000 );
			conn.setReadTimeout( 20000 );

			int i =  ( conn).getResponseCode();
			InputStream is;
			if ( i== 200 || i == 201)
				is = conn.getInputStream();
			else
				is = ( conn).getErrorStream();

			reader = new BufferedReader(new InputStreamReader(is));

			StringBuilder sb = new StringBuilder();
			String line;

			while ((line = reader.readLine()) != null) {
				sb.append( line ).append( "\n" );
			}

			result = sb.toString();
		} catch (Exception ex) {
			Log.e("Exception", "" + ex);
		} finally {
			try {
				reader.close();
			}

			catch (Exception ex) {
				Log.e("Exception", "" + ex);
			}
		}
		return result;
	}

	public static GeneralRespons postMethod(String method, String data, Context context)
			throws UnsupportedEncodingException {

		GeneralRespons respons = new GeneralRespons();
		String result = "";
		BufferedReader reader = null;

		try {

			URL url = new URL(context.getString(R.string.server_url) +  method);

			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestProperty("Content-type", "application/json");
			conn.setRequestMethod("POST");
			conn.setReadTimeout( 60000 );

			OutputStreamWriter wr = new OutputStreamWriter(
					conn.getOutputStream());
			if (data != null)
				wr.write(data);
			wr.flush();

			InputStream is;
			boolean hasError = false;
			int responseCode = (conn).getResponseCode();
			if (responseCode == 200 || responseCode == 201)
				is = conn.getInputStream();
			else {
				is = (conn).getErrorStream();
				hasError = true;
			}

			reader = new BufferedReader(new InputStreamReader(is));


			StringBuilder sb = new StringBuilder();
			String line;

			while ((line = reader.readLine()) != null) {
				sb.append( line ).append( "\n" );
			}

			result = sb.toString();

			if(hasError)
				respons.error = result;
			else
				respons.result = result;
		} catch (Exception ex) {
			Log.e("Exception", "" + ex);
		} finally {
			try {
				reader.close();
			}

			catch (Exception ex) {
				Log.e("Exception", "" + ex);
			}
		}
		return respons;

	}
}
