package com.app.programmingtutorial.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ExamObject implements Serializable {


	private static final long serialVersionUID = -1940039923872227174L;
	public String type;
	public String content;
	public int answer;
	public String title;
	public ArrayList<String> choices;
}